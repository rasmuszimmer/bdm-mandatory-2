﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBLP
{
    public class DblpEntity
    {
        public DblpEntity(string dblpRecordType, string date, string key, string title, short year, List<string> authors, List<string> editors)
        {
            DblpRecordType = dblpRecordType;
            Date = date;
            Key = key;
            Title = title;
            Year = year;
            Authors = authors;
            Editors = editors;
        }

        public string DblpRecordType { get; set; }

        public string Date { get; set; }

        public string Key { get; set; }

        public string Title { get; set; }

        public short Year { get; set; }

        public List<string> Authors { get; set; }

        public List<string> Editors { get; set; }

        public override bool Equals(object obj)
        {
            var item = obj as DblpEntity;

            if (item == null)
            {
                return false;
            }

            return this.Key.Equals(item.Key);
        }

        public override int GetHashCode()
        {
            return this.Key.GetHashCode();
        }
    }
}
