﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBLP
{
    class Researcher
    {

        public Researcher(string name, string jobTitle, List<string> organisations)
        {
            Name = name;
            JobTitle = jobTitle;
            Organisations = organisations;
        }

        public string Name { get; set; }
       
        public string JobTitle { get; set; }

        public List<string> Organisations { get; set; }

        public override bool Equals(object obj)
        {
            var item = obj as Researcher;

            if (item == null)
            {
                return false;
            }

            return this.Name.Equals(item.Name);
        }

        public override int GetHashCode()
        {
            return this.Name.GetHashCode();
        }
    }
}
