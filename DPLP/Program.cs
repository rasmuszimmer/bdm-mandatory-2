﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml;
using System.Xml.Schema;

namespace DBLP
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.SetWindowSize(Console.LargestWindowWidth / 2, Console.LargestWindowHeight / 2);
            List<Researcher> researchersRaw = GetITUResearchers();
            var dblpEntities = GetDblpEntities();

            var researchersDistinct = researchersRaw.Distinct().ToList();
            PerformAnalysis(researchersDistinct, dblpEntities);
        }

        private static void PerformAnalysis(List<Researcher> researchers, IEnumerable<DblpEntity> dblpEntities)
        {
            // Pluck researcher names for easier intersections
            var researcherNames = researchers
                .Select(researcher => researcher.Name)
                .ToList();

            // Create dictionary to help with agrregating collective works per researcher
            var researcherDict = researchers
                .ToDictionary(researcher => researcher.Name, researcher => researcher);

            // Create data structure Dictionary<Researcher, List<DblpEntity>> for mapping entities to faculty members and for further analysis
            var watch = Stopwatch.StartNew();
            Console.WriteLine("Create data structure 'researcherCollectiveWorks' started...");
            Dictionary<Researcher, List<DblpEntity>> researcherCollectiveWorks = new Dictionary<Researcher, List<DblpEntity>>();
            foreach (var dblpEntity in dblpEntities)
            {
                var authIntersect = dblpEntity.Authors.Intersect(researcherNames);
                var editIntersect = dblpEntity.Editors.Intersect(researcherNames);
                var union = authIntersect.Union(editIntersect);

                foreach (var item in union)
                {
                    var researcher = researcherDict[item];
                    if (researcherCollectiveWorks.ContainsKey(researcher))
                    {
                        researcherCollectiveWorks[researcher].Add(dblpEntity);
                    }
                    else
                    {
                        researcherCollectiveWorks.Add(researcher, new List<DblpEntity>() { dblpEntity });
                    }
                }
            }
            watch.Stop();
            var elapsed = watch.Elapsed;
            Console.WriteLine("Create data structure 'researcherCollectiveWorks' finished after {0}", elapsed);

            // DBLP entities where at least one ITU faculty member is author/co-author/editor
            var ituRelatedEntities = researcherCollectiveWorks.Values
                .SelectMany(dblpEntity => dblpEntity)
                .Distinct();
            var ituRelatedEntitiesGroupedByRecordType = ituRelatedEntities
                .GroupBy(dblpEntity => dblpEntity.DblpRecordType)
                .OrderByDescending(grp => grp.Count());

            Console.WriteLine("\n");
            Console.WriteLine(string.Format("DBLP entities where at least one ITU faculty member is author/co-author/editor: {0}", ituRelatedEntities.Count()));
            Console.WriteLine(string.Format("Most represented DBLP Record type: {0}", ituRelatedEntitiesGroupedByRecordType.First().Key));

            // ITU faculty members who have publication records in DBLP
            var ituFacultyMembersWithPublications = researcherCollectiveWorks.Keys;
            var ituFacultyMembersWithPublicationsGroupedByJobTitle = ituFacultyMembersWithPublications
                .GroupBy(researcher => researcher.JobTitle)
                .OrderByDescending(grp => grp.Count());

            Console.WriteLine("\n");
            Console.WriteLine(string.Format("ITU faculty members who have publication records in DBLP: {0}", ituFacultyMembersWithPublications.Count()));
            Console.WriteLine(string.Format("Most represented job title: {0}", ituFacultyMembersWithPublicationsGroupedByJobTitle.First().Key));

            // ITU faculty members who do not have publication records in DBLP
            var ituFacultyMembersWithoutPublications = researchers.Except(ituFacultyMembersWithPublications);
            var ituFacultyMembersWithoutPublicationsGroupedByJobTitle = ituFacultyMembersWithoutPublications
                .GroupBy(researcher => researcher.JobTitle)
                .OrderByDescending(grp => grp.Count());

            Console.WriteLine("\n");
            Console.WriteLine(string.Format("ITU faculty members who do not have publication records in DBLP: {0}", ituFacultyMembersWithPublications.Count()));
            Console.WriteLine(string.Format("Most represented job title: {0}", ituFacultyMembersWithoutPublicationsGroupedByJobTitle.First().Key));

            // List of faculty members ordered by most publications
            var mostPublishedFacultyMembersList = researcherCollectiveWorks.OrderByDescending(kvp => kvp.Value.Count);

            Console.WriteLine("\n");
            Console.WriteLine("List of faculty members ordered by most publications:");
            mostPublishedFacultyMembersList
                .ToList()
                .ForEach(kvp => Console.WriteLine(string.Format("Name: {0}\nTitle: {1}\n Publications: {2}\nOrganisations: {3}\n", kvp.Key.Name, kvp.Key.JobTitle, kvp.Value.Count, string.Join(", ", kvp.Key.Organisations))));
            Console.WriteLine("\n");
        }

        // Display any validation errors.
        private static void ValidationCallBack(object sender, ValidationEventArgs e)
        {
            Console.WriteLine("Validation Error: {0}", e.Message);
        }

        public static List<Researcher> GetITUResearchers()
        {
            var watch = Stopwatch.StartNew();
            Console.WriteLine("GetITUResearchers started...");

            // Create a request for the URL. 
            WebRequest request = WebRequest.Create(
              "https://pure.itu.dk/portal/en/persons/search.html?search=%20&keyword=&current=true&organisationName=&uri=&organisations=&page=0&pageSize=500");
            // If required by the server, set the credentials.
            request.Credentials = CredentialCache.DefaultCredentials;
            // Get the response.
            WebResponse response = request.GetResponse();
            // Display the status.
            Console.WriteLine(((HttpWebResponse)response).StatusDescription);
            // Get the stream containing content returned by the server.
            Stream dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();
            // Clean up the streams and the response.
            reader.Close();
            response.Close();

            List<Researcher> researchers = new List<Researcher>();
            researchers.Capacity = 2800000;

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(responseFromServer);

            if (doc.ParseErrors != null && doc.ParseErrors.Count() > 0)
            {
                // Handle any parse errors as required

            }
            else
            {

                if (doc.DocumentNode != null)
                {
                    HtmlNodeCollection nodes = doc.DocumentNode.SelectNodes("//div[contains(concat(' ', @class, ' '), ' rendering_person_short ')]");

                    foreach (var node in nodes)
                    {
                        //Get names
                        var nameNode = node.SelectSingleNode("h2[contains(concat(' ', @class, ' '), ' title ')]");
                        var nameString = nameNode == null ? string.Empty : nameNode.InnerText;
                        var names = nameString.Split(',');
                        var firstName = names[1].Trim();
                        var lastName = names[0].Trim();
                        var formattedName = string.Format("{0} {1}", firstName, lastName);

                        //Get job title
                        var jobTitleNode = node.SelectSingleNode("p[contains(concat(' ', @class, ' '), ' jobtitles ')]");
                        var jobTitleString = jobTitleNode == null ? string.Empty : jobTitleNode.InnerText;

                        //Get organizations
                        var organisationContainerNode = node.SelectSingleNode("ul");
                        HtmlNodeCollection organisationNodes = null;
                        if (organisationContainerNode != null)
                        {
                            organisationNodes = organisationContainerNode.SelectNodes("li");
                        }
                        var organisations = new List<string>();
                        if (organisationNodes != null)
                        {
                            foreach (var organisation in organisationNodes)
                            {
                                organisations.Add(organisation.InnerText);

                            }
                        }

                        //Create researcher object
                        var researcher = new Researcher(formattedName, jobTitleString, organisations);
                        researchers.Add(researcher);
                    }
                }
            }

            watch.Stop();
            var elapsed = watch.Elapsed;
            Console.WriteLine("GetITUResearchers finished after {0}", elapsed);

            return researchers;
        }

        private static IEnumerable<DblpEntity> GetDblpEntities()
        {
            var watch = Stopwatch.StartNew();
            Console.WriteLine("GetDblpEntities started...");

            // Set the validation settings.
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.DtdProcessing = DtdProcessing.Parse;
            settings.ValidationType = ValidationType.DTD;
            settings.ValidationEventHandler += new ValidationEventHandler(ValidationCallBack);

            // Create the XmlReader object.
            using (XmlReader reader = XmlReader.Create("dblp.xml", settings))
            {

                //Parse the file. 
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        switch (reader.Name)
                        {
                            case "article":
                            case "inproceedings":
                            case "proceedings":
                            case "book":
                            case "incollection":
                            case "phdthesis":
                            case "mastersthesis":
                                string dplbRecordType = reader.Name;
                                string date = reader.GetAttribute("mdate");
                                string key = reader.GetAttribute("key");
                                string title = "";
                                short year = 0;
                                List<string> authors = new List<string>();
                                List<string> editors = new List<string>();


                                while (!(reader.NodeType == XmlNodeType.EndElement && reader.Name == dplbRecordType))
                                {
                                    reader.Read();
                                    if (reader.NodeType != XmlNodeType.EndElement)
                                    {

                                        try
                                        {
                                            switch (reader.Name)
                                            {
                                                case "author":
                                                    var author = reader.ReadElementContentAsString();
                                                    authors.Add(author);
                                                    break;
                                                case "editor":
                                                    var editor = reader.ReadElementContentAsString();
                                                    editors.Add(editor);
                                                    break;
                                                case "title":
                                                    title = reader.ReadElementContentAsString();
                                                    break;
                                                case "year":
                                                    year = (short)reader.ReadElementContentAsInt();
                                                    break;
                                                default:
                                                    break;
                                            }
                                        }
                                        catch (XmlException e)
                                        {
                                            Console.WriteLine(e.Message);
                                            break;
                                        }
                                    }
                                }

                                DblpEntity entity = new DblpEntity(dplbRecordType, date, key, title, year, authors, editors);
                                yield return entity;

                                break;
                            default:
                                break;
                        }
                    }

                }
            }

            watch.Stop();
            var elapsed = watch.Elapsed;
            Console.WriteLine("GetDblpEntities finished after {0}", elapsed);
        }
    }
}
